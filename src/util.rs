use crate::{Attestation, SignedAttestation};
use bitcoin::secp256k1::{All, Message, Secp256k1};
use bitcoin::KeyPair;

/// Sign an attestation
pub fn sign_attestation(
    attestation: Attestation,
    keypair: &KeyPair,
    secp: &Secp256k1<All>,
) -> SignedAttestation {
    let message = Message::from_slice(&attestation.hash()).expect("sign message");
    let signature = secp.sign_schnorr_no_aux_rand(&message, &keypair);
    SignedAttestation {
        attestation,
        signature,
    }
}
